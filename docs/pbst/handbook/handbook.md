---
title: Pinewood Builders Security Team Handbook
---

<div align="center"> 

![PBSTIcon|150x150, 100%](/pbst-logo.png)

# [Pinewood Builders Security Team](https://www.roblox.com/groups/645836/Pinewood-Builders-Security-Team)

</div>

The following document is the PBST handbook, written by the Trainers for all PBST members. Please read it carefully.

Failure to follow the handbook may lead to punishments, ranging from a simple warning to a demotion or even a blacklist.

Updates to this handbook will be specified.

**Last Updated on Jan 3rd, 2022**

**Peace through Strength!**

---
## Basic duties

* Protect the core from melting/freezing down
* Keep players safe from mutants, OP Weapon attackers, and disasters
* Patrol the facilities for exploiters and Mass Random Killers, and call PIA if these are present

## Basic Rules

1. Follow the [ROBLOX Community Rules](https://en.help.roblox.com/hc/en-us/articles/203313410-Roblox-Community-Rules) and the [Pinewood Builders Game Rules](https://devforum.roblox.com/t/pinewood-builders-game-rules/907637) **at all times**. Exploiting and glitching, in particular, is a big no-no.

2. Wear your uniform and rank tag while on duty. You can only take PBST Weapons if you're wearing these.
2a. To enter the core, you must put on a Hazmat suit, then equip the lanyard with the command `!PBSTHazmat`
2b. Don't become a mutant while on duty, other PBST will kill you immediately.
3. Use your weapons responsibly when on duty.
3a. Don't use PBST Weapons to randomly kill players or to cause a melt-or freezedown, unless TMS is hosting a reverse raid or a Trainer gives a direct order.
3b. You can't use tools & weapons from other Pinewood subgroups (like PET or TMS) when on duty as PBST. Make sure they're not in your inventory.
3c. If you use non-PBST weapons on duty (like the OP Weapons of the game pass or credit-earned guns), you have to follow the same rules as if they were PBST Weapons.
4. Always give people a warning, and only engage in combat if they don't listen.
4a. This rule has exceptions, they are listed further down.
5. Don't 'revenge kill', i.e. killing someone just because they killed you.
6. TMS with the red ranking and black uniform are to be killed on sight.
6a. TMS with only the rank tag but no uniform may be killed without warning if they try to destroy the core.
6b. Players with a TMS uniform but not the rank tag are to be treated as regular visitors.
7. PBCC's spawn area, the PET HQ, the PBST room, and the TMS hideout are no-kill zones for PBST and TMS. Everyone should be allowed to take uniforms and loadouts without being insta-killed the moment they exit.
7a. Don't run to these areas to try to get out of combat mid-fight and claim a "no-kill zone".

## Reminders to all PBST members

* Do not try to evade punishment. Doing so will only make it worse for yourself.
* Do not steal any assets that relate to Pinewood Builders or its members, including but not limited to; Game assets, uniforms, and community-made artwork. **You will be blacklisted if you refuse to remove stolen assets.**
* You are not allowed to share/sell accounts with PBST points/ranks.
* Follow orders from your superiors and be respectful to your fellow players. Don't act like you're the boss of the game.
* Glitching is severely frowned upon, doing this at events can get you on KoS for all sides until the event ends.
* **Trainers can remove a user from a PBST position for poor behavior,** both in the communications server and in-game. This includes toxicity, inappropriate behavior, etc. Anyone who acts in ways not suitable for his/her rank will see that rank revoked.

---
## Going on duty

To be recognized as an on-duty Security member, you have to meet a few requirements, otherwise, you're not allowed to use PBST Weapons.

You'll need an official PBST uniform, buy one in the [PBST group store](https://www.roblox.com/groups/645836/Pinewood-Builders-Security-Team#!/store) or use a uniform giver in the game. You need to wear both a shirt & pants.

Make sure that your avatar allows for a visible uniform, don't use packages that hide clothes. 

Using PET Suits are only allowed in specific cases:

* Hazmat suit: Use the PBST lanyard. Equip the Hazmat Suit, then use the command `!pbsthazmat`.
* Fire Suit: Wear a PBST uniform over it. Equip the Fire Suit, then return to the uniform givers (or use donor commands).
* Medical Suit: Not allowed.

Also, set your ranktag to Security using the in-game command `!setgroup PBST`. New players may be unfamiliar with the ranktags and have theirs on Security without intending to be on duty. Do not warn them about this.

To go off-duty, remove your uniform and PBST weapons (reset your character if required). Remove your Security ranktag as well using the command `!setgroup PB/PET/PBQA...` to change to any other PB group you're in, or use `!ranktag off` to turn it off entirely.

---

## Patrolling the Computer Core

Pinewood Computer Core is Pinewood Builders' most frequently visited game, making it the main place of interest for PBST to patrol. Our main purpose is to protect the core from melting or freezing down.

<p>
<details>
<summary>Controlling the temperature</summary>

PBST's main goal in this game is to prevent the core from melting or freezing. Work together to make sure the core stays in balance. The following chart shows how different settings affect the temperature: 

![Stable core combos|690x298](/core-chart.png)

To keep the core stable, you can use one of several combos.

![Core chart|432x500](/core_temps.png)

</details>
</p>

<p>
<details>
<summary>Coolant</summary>

The coolant is the most powerful and most vulnerable core control, and everything needs to be right to keep it going.

* Coolant pumps must be on.
* Coolant production must be active, as the pumps will shut down without coolant in the tank. Trains can also supply additional coolant.
* In Admin control, the coolant supply button must be turned on.

Additionally, the coolant pipe at the mainframe can be sabotaged, reducing its cooling effect to -12°. If the coolant works beneficial to PBST, you can shoot people who sabotage the coolant without warning. However, if the core is approaching freezedown, PBST can sabotage the coolant as well to stabilize the core.

![Sabotaged coolant pipe|690x343, 50%](/sabotaged-coolant-pipe.jpg)
</details>
</p>

<p>
<details>
<summary>Emergency Coolant</summary>

If a meltdown does occur, go to the Emergency Coolant at Sector G to try to rescue the core. Use the Security Code to get in (announced in-game) and bring the rods to levels between **69% and 81%**. Keep as many rods as possible within these levels until the timer hits 0.

Success rates for the number of coolant rods green when the countdown hits 0: <br>
1: 10% <br>
2: 25% <br>
3: 90% <br>

Successful activation of the E-coolant will set the temperature back to 3500 degrees.

---
</details>
</p>

Aside from melt- and freezedown, PBST may also need to evacuate visitors during a plasma surge, radiation flood or magma rising. Try to help out everyone who needs guidance.

If you see a green, damaging, growling abomination, it's a mutant, kill it at once. Remember rule 2b, PBST members may not become mutants while on duty.

Spawn killing is generally against the game rules and only allowed in self-defense, an exploiter, or someone you were already in combat with. PBCC has protection in place to prevent spawn killing, which extends through the shop and stairwell and ends past the yellow line at the bottom of the elevator. Camping right outside the spawn killing protection and blocking it is severely frowned upon, PIA may consider this spawn killing as well.

<p>
<details>
<summary>Trains</summary>

PBST can clean up derailed trains if they're blocking the tracks, preventing other trains from going by. Find the driver's seat and sit on it, then click "remove train". You are free to use your taser if somebody is still in the driver's seat and refuses to remove the train themself.

Nuke trains are an additional danger since the nukes can explode, killing everyone nearby. Remove these trains as quickly as possible if any nukes are flashing red. If you see someone trying to explode a nuke train on purpose, you can kill that person without warning.

Trains and other vehicles can be flown to places they shouldn't be at. If you see someone flying a vehicle, you can use your taser to stop them. Remove any vehicle that has been flown.

</details>
</p>

---

## Usage of weapons
If you see anyone changing the core controls to invoke a melt- or freezedown, give them a warning (for example by using your taser).

If they ignore your warning and change the controls a **second time, you are allowed to kill them**.

On a few occasions, you can kill players *without needing to warn them first* if they:
* have been warned and killed for changing the core controls to melt- or freezedown (or breaking other rules), and still come back to do the same
* were hostile to Security, and are now changing the core controls to melt- or freezedown
* are spamming the reactor power button at PBCC, trying to get it locked
* are sabotaging the coolant pipe (or fixing it if the core is approaching freezedown)
* have the TMS ranktag who are changing the core controls to melt- or freezedown (even if they aren't wearing the TMS uniform)
* are setting trains on fire, or trying to ignite nuke trains to explode
* **Only when the core is less than 1000° from melt- or freezedown**, are setting the core controls disadvantageously to PBST
* **Only during melt- or freezedown**, are messing with the emergency coolant and rockets at PBCC

Aside from the core controls, you are expected to use your weapons responsibly (see Rule 3). Self-defense is allowed, and protecting visitors or fellow PBST from attackers is also okay. 

<p>
<details>
<summary>Tasers</summary>

Tier 1 and higher get tasers, a non-lethal weapon to stun your enemies. They can use this to:
- give warnings
- stop players who need to be killed, before killing them
- take care of players who refuse to remove trains, and stop train flyers

Players only need to be tased once to get stunned. Tasing players multiple times or using the taser for other things can be considered weapon abuse, and be handled accordingly (see the Rogue PBST chapter).
</details>
</p>

<p>
<details>
<summary>Kill on Sight (KoS)</summary>

Any person on KoS is to be killed immediately upon seeing them. **TMS, exploiters, and mutants of PBCC are on KoS by default.** At the PBST Activity center, players on the Raiders team are KoS for PBST as well.

If you attack an exploiter, make sure somebody is recording it so the PIA will have evidence to ban them.

Only SD+ can place somebody on KoS for all PBST to follow.

During TMS Raids and PBST Patrol events, an Elite Tier+ can place KoS orders which last for the duration of the raid. An SD+ can override this order if they join.

Specific actions can get you placed on KoS for all sides during TMS raids, more on that in the corresponding chapter.
</details>
</p>

<p>
<details>
<summary>Room restriction</summary>

In general, all facilities are accessible to all players. Unless you know a player will be hostile or attempt to melt/freeze the core, do not prevent them from entering.

Only an SD+ is allowed to order a room to be restricted. Field Tiers are also authorized to order room restrictions during TMS Raids & patrols.

If an SD+ or authorized Field Tier orders to restrict a room, only on-duty PBST will be allowed in there. All non-PBST will be ordered to leave immediately, and killed if they refuse to go.
</details>
</p>

---

## How to deal with...

<p>
<details>
<summary>Rogue PBST</summary>

If a Security member is found breaking a rule in the handbook, alert them of their wrongdoing and give them a warning. If said Security member doesn't listen or actually use their weapons irresponsibly, kill them.

If the rogue PBST is acting way out of line, you can use the Comms server to send a handbook violation report. Make sure you have evidence of the user breaking the rules, and being warned for it.

If a **Tier or higher** is being abusive with their weapons, record the evidence and send it to an SD+.
</details>
</p>
<p>

<details>
<summary>Exploiters</summary>

Exploiting is using any external program to alter gameplay, like autoclicker, glitching, and hacking. It is against Roblox Terms of Service.

If you see an exploiter, call PIA with the `!call` command so they can be banned. If you can, record them exploiting so the PIA will have evidence to ban them. You are free to attack and kill the exploiters until they are banned. Should the exploiter leave before PIA arrive, give the recording to the PIA so that they still have the evidence they need to ban the exploiter.
</details>
</p>
<p>

<details>
<summary>The Mayhem Syndicate (TMS)</summary>

The Mayhem Syndicate is the opposite of PBST. Where we intend to protect the core, they intend to melt or freeze it. They often join a game in a raid, and when this happens you are advised to call for backup.

TMS can be recognized by the red ranktag and the black uniform. Any TMS with ranktag & uniform is Kill on Sight (KoS) by default. TMS with the ranktag and a core suit or any PET suit are KoS as well.

![TMS on KoS|690x298, 50%](/TMS-KoS.jpg)

TMS Inquisitors & Instructors are always KoS, they don't need to wear a uniform. 
Players from other TMS ranks with only the ranktag but no valid uniform may be killed without warning if they actively try to destroy the core. <br>
Players with the TMS uniform but not the ranktag are **not** KoS, treat them as any other visitor.

The only place where TMS may not be killed is at the TMS loadouts near the cargo trains. You have to give TMS a fair chance to take their loadouts, as they will do the same for PBST loadouts. **Do not camp at the TMS loadouts.**

![TMS hideout|690x360, 50%](/TMS-base.jpg)

As with all armed hostiles, you are allowed to fight back if TMS attacks you, even if they seem to be on their way to get their loadouts. Also, if a TMS tries to run for the loadout room and call "protection", you can still kill them as rule 7a states.

More information about TMS Raids is in the chapter below.

TMS also has its own handbook for more information on how they operate: [The Mayhem Syndicate Handbook](https://devforum.roblox.com/t/the-mayhem-syndicate-handbook/595758)
</details>
</p>

---

## Points and ranking up

To rank up in the group, you need PBST points, which you can earn in various ways. You can view your point stats with the in-game `!mypoints` command, or in the Points Room of the [Pinewood Builders Data Storage Facility](https://www.roblox.com/games/1428153850/Pinewood-Builders-Data-Storage-Facility). 

##### **Note that ST Exp and Patrol Time of the Activity Center are not the same as PBST points, nor are PBCC Credits.**

You can find the scheduled upcoming PBST events in the PBST room at PBCC, and at the [Pinewood Builders Data Storage Facility](https://www.roblox.com/games/1428153850/Pinewood-Builders-Data-Storage-Facility). Keep an eye on the group shout as well.

<p>
<details>
<summary>Cadet (DEFAULT)</summary>

Cadet is the default rank, given on joining the group. They have access to a protective riot shield and a very low damage baton.

</details>
</p>

<p>
<details>
<summary>Tiers</summary>

The medium ranks of PBST, these players are the main force of PBST, protecting the facilities with their weapons.

**Requirements**
* Tier 1: 75 points and passing Tier evaluation (see the Becoming a Tier chapter for more information).
* Tier 2: 200 points.
* Tier 3: 400 points.
* Advanced Tier: 600 points.

**Loadout given**
* Tier 1: Low damage baton, riot shield, taser, PBST pistol.
* Tier 2: Mid damage baton, riot shield, taser, PBST pistol, PBST rifle.
* Tier 3: High damage baton, riot shield, taser, PBST pistol, PBST rifle, PBST Submachine gun.
* Advanced Tier: High damage baton, riot shield, taser, PBST pistol, PBST rifle, PBST Submachine gun, gravity disruptor.

**Duties & Perks**
* Ability to call PBST backup using !call (use this wisely!)
* *Comms server only*: Access to reward requests, where you can request bonus points for helpful PBST you saw in-game. Supply evidence!
* Be a role model to Cadets, rule violations may lead to a larger punishment. **You have been warned.**

---

</details>
</p>

**NOTE: All the higher ranks are required to be in the Communications server since they have specific duties that rely on it.**

<p>
<details>
<summary>Elite Tiers</summary>

Handpicked by Trainers, Elite Tiers are in charge of leading their fellow Tiers and Cadets to victory over TMS. They are PBST's role models.

**Requirement**: Handpicked from Advanced Tiers. Current ET's and above can suggest Advanced Tiers for Elite Tier. Trainers will vote on their promotion.

**Loadout given**: High damage baton, riot shield, taser, PBST pistol, PBST rifle, PBST Submachine gun, gravity disruptor, heal shield.

**Duties & Perks**
* **Main duty**: oversee TMS raids: lead PBST, log their attendance and hand out points accordingly (See the TMS raids chapter for more information).
* Order a Kill on Sight **during TMS Raids and Patrols**. All PBST get free reign to kill that player whenever they see them, except in places that would count as spawn killing.
* Access to a custom uniform with the !et1 command.

---

</details>
</p>
<p>

<details>
<summary>Field Tiers</summary>

Handpicked by Trainers from the best Elite Tiers, Field Tiers enjoy the highest trust and additional perks. They can host patrol events where PBST will go out and protect the Computer Core.

**Requirement**: Handpicked from Elite Tiers. ET's and above can suggest Elite Tiers for Field Tier. Trainers will vote on their promotion.

**Loadout given**: High damage baton, riot shield, taser, PBST pistol, PBST rifle, PBST Submachine gun, gravity disruptor, heal shield, sentry.

**Duties & Perks** (similar to Elite Tier, with additions)
* **Main duty**: oversee TMS raids: lead PBST, log their attendance and hand out points accordingly (See the TMS raids chapter for more information).
* **Main duty**: host patrols with permission from a Trainer. They take a group of players on patrol in a PB facility, and simulate the experience for points. (See the Patrols chapter for more information).
* Order a Kill on Sight **during TMS Raids and Patrols**. All PBST get free reign to kill that player whenever they see them, except in places that would count as spawn killing.
* Order room restrictions **during TMS Raids and Patrols**. Only on-duty PBST will be allowed to enter a restricted room.
* Access to a custom uniform with the !et2 command.

---

</details>
</p>
<p>

<details>
<summary>Special Defense</summary>


**Requirements**
* Be in the Communications server
* Have 2-step verification on all relevant connected accounts
* 750 points
* Be selected for an SD Evaluation: host a PBST Training and your performance will be closely watched

**Loadout given**: High damage baton, riot shield, taser, UPG pistol, UPG rifle, UPG Submachine gun, katana.

**Duties & Perks**
* Order room restrictions and KoS at any time
* Uniform optional, not required
* Kronos mod at training facilities (use this wisely!)
* Host trainings, tier evals, mass patrols, etc. with permission from a Trainer. Events aren't allowed to overlap with each other. SD's can "take" slots previously occupied by ET+.
* **Primary duty**: hosting (if granted permission) and raid response leading.
* Reward/Punishment requests for lower ranks. SD's are encouraged to do this but they aren’t exactly forced to. Though, if an SD is found purposefully ignoring an extreme case over favoritism, he/she will be warned/punished.
* *Secondary duty*: moderate the Comms server.

---

</details>
</p>
<p>

<details>
<summary>Trainers</summary>


**Requirements**
* Trainer vote on your promotion from SD to Trainer
To become a Trainer, all the current Trainers have to vote on your promotion. Only SD's are eligible for this promotion.

**Primary duties**
* Participation in votes. All Trainers should cast their vote most of the time. If a Trainer has no opinion, they can abstain.
* Handling operations, promotions, points, and is responsible for all changes done to the group.
* Hosting events.

**Secondary duties**
* Moderate the Comms server
* Enforce the handbook in-game

**Perks**
* Can maintain their rank when on extended leave, or leave and have their rank restored upon returning.
* Overlap events with other, previously scheduled events.

</details>
</p>

---

## Patrols & TMS Raids

The most recommended ways to earn points are attending scheduled patrols and responding to TMS Raids. Since these patrols will often prompt responses from TMS in a "patrol raid", a number of subgroup-wide rules are in place to keep things organized.

If you're already on duty in a PBCC server when a raid/patrol starts there, you can participate right away. If you don't want to participate, go off-duty (read the corresponding chapter above).

If you're off-duty when an event begins and you want to participate, you have to pick a side and fight on that side until the raid ends. **Do not change sides mid-event.** This also applies if you are neutral or off-duty and decide to participate anyway. Beware that choosing to side with either TMS or PBST in a raid/patrol might get you put on KoS by the other side until the event ends.

If TMS has achieved a melt- or freezedown, they may do a "Yes-Survivor" run and help out civilians to escape in the rockets. Their KoS will be lifted if they do this, and standard warning rules apply instead (Rule 4 and its exceptions).

Using glitches is not allowed in raids & patrols. Doing this, even as a neutral, will get you put on KoS **by all sides until the raid ends**. Any game mechanic that was unintentionally left in counts as a glitch.

Repeated rulebreaking, toxicity or general troublemaking may get you **blacklisted** from raids & patrols, you'll be on global KoS during all raids & patrols in public servers. If a raid/patrol is hosted in private server, you'll be banned from it.

<p>
<details>
<summary>TMS Raids</summary>

TMS raids are organized attacks hosted by TMS Captains and above. Elite Tiers+ will call PBST to respond to them, and monitor everyone's performance to give points accordingly. They may do periodic checks to see who is attending on PBST's side.

The TMS raids have levels assigned to them, between 0 and 3. This level determines the base points PBST earn when responding to this raid. Players with outstanding performance or leadership can earn up to 2 bonus points. However, players who only attend part of the raid or perform poorly get fewer points.

Level 0 raid (Practice raid) : 2 base points, only one person can get only one bonus point. <br>
Level 1 raid : 4 base points <br>
Level 2 raid : 6 base points <br>
Level 3 raid (Mega raid) : 8 base points 

---

</details>
</p>

<p>
<details>
<summary>Patrols</summary>

Hosted patrols aim to simulate the experience of patrolling Pinewood Computer Core in a group. The host and their assistants will let people join a PBCC server through their profile and monitor how everyone does.

Like TMS raids, patrols are split into levels that determine the base points PBST earns when attending. Players with outstanding performance or leadership can earn 1 bonus point. However, players who only attend part of the raid or perform poorly get fewer points. Practice patrols have 0 base points, so you can only earn a point there as a bonus.

Patrols, like other events, can only be scheduled with permission from a Trainer. Field Tiers and above can request to host L0-L2 patrols, but Mega patrols can only be scheduled by SD+.

Level 0 patrol (Practice patrol) : 0 base points, 20-30 minutes. <br>
Level 1 patrol : 2 base points, 30-60 minutes. <br>
Level 2 patrol : 4 base points, 1 hour or longer. <br>
Level 3 patrol (Mega patrol) : 8 base points, 1 hour or longer, similar to Mega-Training.

</details>
</p>

---

## Trainings

SD's and Trainers regularly host trainings, which are an alternative way to earn points. **You can earn up to 5 points in a normal training.** The better you perform, the more points you get.

Make sure to join the training in time (preferably 5-10 minutes before the scheduled starting time). If the server is full, another host is allowed to step in and host at a second server, possibly in another training facility at the host's decision.

<p>
<details>
<summary>Training rules</summary>

* Listen to the host and follow their orders.
* Do not take your loadouts, the host will give you what you need in every activity.
* Always wear a PBST uniform in the training.
* While anyone can assist a training, do not ask to be an assistant.
* If you are selected to assist, you may be given temporary admin powers which are **strictly**  for that specific training. Abuse of these powers will result in severe punishment.
* You can use the command `!traininginfo` at any PB facility to get info about frequently used terms in trainings, and information about PTS.

</details>
</p>

On top of the normal trainings, there can be a few other types of training:
* Mega-Training, every other Saturday at 5:00 PM UTC. You can earn double points there (up to 10).
* Disciplinary Training (DT), which is stricter and more focused on discipline. You'll be kicked from the training if you misbehave. Points are handed out normally (up to 5).
* Super Training, Hosted on special occasions. You can earn double points there (up to 10).

Keep an eye on the scheduler for any of these trainings.

---

## Scheduling rules

Events can only be scheduled with permission from a Trainer.

Field Tiers can only schedule patrols. They have a few restrictions:
* One FT can only host one patrol per day.
* When scheduling a patrol, its beginning and end has to leave a gap of at least 1 hour between the beginning & end of previously scheduled events.
* FT's can't schedule on top of other events.

SD's can schedule patrols, trainings, DT's and Tier Evaluations. No time gap is required, but the event they schedule can't overlap with previously scheduled events.

Trainers can schedule all events, no time gap is required. Events they schedule are allowed to overlap with other events.

MEGA events are the only exception: where a 1 hour gap before and after the event must be maintained at all times. These events include PBST Mega Trainings, TMS Mega Raids and PET Operation MEGA's.

---

<p>
<details>
<summary>Self-training</summary>

If no Trainings or TMS raids are happening or scheduled, you can go to Sector 2 of the Activity Center to train yourself. There are several activities to choose from, and you can earn up to 3 points depending on your rank and the difficulty you choose.
</details>
</p>
<p>

<details>
<summary>Self-patrol (COMMS SERVER ONLY)</summary>

You can record yourself patrolling a facility, and post it in the Comms server. The recording needs to be between 30 minutes and 2 hours, and there is a 24h cooldown between submissions.

The amount of points you can earn depends on how long you patrolled and how active you were in that time. The **maximum** points you can get for self-patrol are: 
* 30 min → 2
* 60 min → 3
* 90 min → 4
* 120 min → 6
---

</details>
</p>

Once exported, a Trainer needs to log the points, and handle any promotions. This may take some time, so **be patient. Do not ask Trainers to log your points or promote you.**

<p>
<details>
<summary>Tier evaluations</summary>

Once you reach 75 points, you need to take the Tier evaluation to become Tier 1. It is not recommended to get more points before your evaluation, if you take the eval while having more than 150 points you'll be set back to 150.

As with normal trainings, the scheduled Tier evaluations can be found on the schedule at [Pinewood Builders Data Storage Facility](https://www.roblox.com/games/1428153850/Pinewood-Builders-Data-Storage-Facility). They will be hosted regularly to accommodate all timezones. **There are no points to be earned in a Tier eval.**

A Tier evaluation consists of 4 parts: a quiz, a combat eval, a test on patrolling skills and a consensus. Evals are hosted at a dedicated server at the PBST Activity Center, accessible with the command `!pbstevalserver` if you have 75+ points. If you pass some parts part but fail others, you may try again at another eval and skip any part you passed before.

* For quiz & combat you can request a 1-on-1 eval, PM RogueVader1996, superstefano4, TheSparkedFlame, TheUnrealObama, Phantom_FR, neonweld, SnowFrosm, resupero or SourLem0n4.
* The quiz also has [a separate game](https://www.roblox.com/games/7193747285/Tier-Evaluation-Quiz) where you can pass this part.
* The patrol part is covered in scheduled evaluations. These can also include other parts.
* The consensus part is handled by HR's, they require no action from the attending Cadets.

The quiz is 8 questions about this handbook, training rules, and more. You need to score at least 5/8 to pass. You can take the quiz in [this game](https://www.roblox.com/games/7193747285/Tier-Evaluation-Quiz) or request a 1-on-1 eval.

The combat eval consists of 3 challenges: a Level 1 SF Bot Arena, Juggernaut dodging challenge with increasing levels, and Firing Range where you have to get as many hits as you can within 60 seconds. Each completed challenge will give combat points, you need 3 combat points to pass this part.

![75%](/tier-eval-1.png)
![75%](/pbst-tier-eval.png)

In the patrol test, you will receive Tier 1 loadouts and be tested on skills like your abilities in combat, teamwork, and following the handbook correctly. The Tier eval assistants will present you with various situations where you have to react to appropriately.

After passing the patrol test, you need to wait and get consensus from the Elite Tiers and above. If you don't pass this, you'll get a cooldown period which can last between a week and 3 months.

<p>
<details>
<summary>Assisting a Tier eval</summary>

**NOTE: Assisting Tier evals will be done using a Communications server, which is therefore required.**

Tier 1+ can volunteer to assist a Tier eval through the comms server. Their most important task is to play out all the scenarios in the patrol part of the eval. They also help out the host with the usual stuff.

Assisting a Tier eval will get you points depending on how long you assisted. **Only the assistants can earn points in the Tier eval.** <br>
30 Minutes - 3 points <br>
1 Hour - 5 points <br>
1 Hour and 30 minutes - 7 points <br>
2 Hours - 10 points <br>
2 Hours and 30 minutes - 13 points <br>
3 Hours - 15 points <br>

</details>
</p>

</details>
</p>

Signed, <br>
PBST Leadership
