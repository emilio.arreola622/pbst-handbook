# Trainings and ranking up

## Trainings 
SD’s and Trainers regularly host trainings, which are an alternative way to earn points. **You can earn up to 5 points in a normal training.** The better you perform, the more points you get. The schedule of these trainings can be found at the [Pinewood Builders Data Storage Facility](https://www.roblox.com/games/1428153850/Pinewood-Builders-Data-Storage-Facility). Keep an eye on the group shout as well.

On top of the normal trainings, there can be a few other types of events:

 * Mega Training, every Sunday at 5:00 PM UTC. You can earn double points there (up to 10).
 * Disciplinary Training, which is stricter and more focused on discipline. You’ll be kicked from the training if you misbehave. Points are handed out normally (up to 5).
 * Hardcore Training, where even the slightest mistake will get you kicked. If you ‘survive’ to the end, you can earn up to 20 points.
 * Super Training, Hosted on special occasions. You can earn double points there (up to 10).
 * Keep an eye on the scheduler for any of these trainings.

### Training Rules
 * Listen to the host and follow his/her orders.
 * Join the server in time (preferably 5-10 minutes before the scheduled starting time), the host will whitelist and you won’t be able to get in if you’re late.
 * Do not take your loadouts, the host will give you what you need in every activity.
 * Always wear a PBST uniform in the training.
 * While anyone can assist a training, do not ask to be an assistant.  
 * You can use the command !traininginfo at any PB facility to get info about frequently used terms in trainings, and information about PTS.  

## Patrols
Hosted patrols aim to simulate the experience of patrolling facilities in a group. The host and their assistants will let people join a server through their profile and monitor how everyone does.

Patrols, like other events can only be scheduled with permission from a Trainer.

Similar to TMS raids, patrols are split into similar levels:
Level 0 patrol: 0-1 points, 30 minutes to an hour. Hosted by Elite Tier+.
Level 1 patrol: 1-3 points, 1-2 hours. Hosted by Elite Tier+.
Level 2 patrol (Mass patrol): 1-5 points, hosted by SD+, similar to training.
Level 3 patrol (Mega patrol): 1-10 points, hosted by SD+, similar to Mega Training.

Patrols hosted by ET+ have a few restrictions:
 * There can only be three patrols per day.
 * One ET can only host one patrol per day.
 * ET's can only schedule 2 days ahead of time.
 * There has to be at least an hour in between the end of one patrol and the start of another.
 * ET’s can’t schedule on top of other events.

 ## Self-patrol (COMMS SERVER ONLY)
You can record yourself patrolling a facility, and post it in the Comms server to get a maximum of 5 points. Depending on the time, your actions, etc. in the recording, the points may vary.

The recording needs to be between 30 minutes and 2 hours, and there is a 24h cooldown between submissions.

## Self-training (TIERS ONLY)
***NOTE: Self-Training is only available to Tiers, as Cadets need to get familiar with the training discipline and patrols before they flesh out their skills.***

If no Trainings or TMS raids are happening or scheduled, you can go to Sector 2 of the Activity Center to train yourself. There are several activities to choose from, and you can earn up to 3 points depending on your rank and the difficulty you choose.

Points, once exported, will have to be manually logged by a Trainer. The same applies for promotions. This may take some time, so **be patient. Do not ask Trainers to log your points or promote you.**

## TMS Raids
TMS raids are the most recommended way to earn points. If they start raiding a server to cause a melt- or freezedown, you can earn points quite easily by fighting against them. Make sure to keep an eye out for when they start joining your game.  

The TMS raids have levels assigned to them, between 0 and 3. These determine the severity of the raid, and give an indication of how many TMS will be there. Level 3 raids (aka Mega raids) are the rarest, and involve TMS raiding multiple servers at once.  

The amount of points that can be earned depends on the TMS raid level. The level determines the base points that PBST can earn when responding to this raid. Players with outstanding performance or leadership can earn up to 2 extra points. However, players who only attend part of the raid or perform poorly get less than the base points.  

- Base points for Level **0** (Practice raid): 2  
- Base points for Level **1**: 4  
- Base points for Level **2**: 6  
- Base points for Level **3** (Mega raid): 8  

Elite Tiers will supervise the raid and monitor everyone’s performance to give points accordingly. They may do periodic checks to see who is attending on PBST’s side, so make sure you follow whatever instructions they give you.  
